import React from "react";
import ReactDOM from "react-dom";
import { connect } from "react-redux";

class Modal extends React.Component {
  render() {
    console.log(this.props);
    return ReactDOM.createPortal(
      <div
        className="ui dimmer modals visible active"
        onClick={this.props.onDismiss}
      >
        <div
          className="ui standard modal visible active"
          onClick={(e) => e.stopPropagation()}
        >
          <i className="close icon" onClick={this.props.onDismiss}></i>
          <div class="ui link cards">
            <div class="ui centered card">
              <div class="image">
                <img src="/resources/images/profile.png" />
              </div>
              <div class="content">
                <div class="header">{this.props.profile.user_name}</div>
                <div class="meta">
                  <a>{this.props.profile.email}</a>
                </div>
                <div class="description">
                {this.props.profile.user_type}
                </div>
              </div>
              <div class="extra content">
                <span class="right floated">{this.props.profile.user_id}</span>
              </div>
            </div>
          </div>
        </div>
      </div>,
      document.querySelector("#modal")
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

export default connect(mapStateToProps, {})(Modal);
