
from flask import Blueprint, request, jsonify
from werkzeug.security import generate_password_hash
from werkzeug.security import check_password_hash


from flask_jwt_extended import create_access_token
from flask_jwt_extended import set_access_cookies
from flask_jwt_extended import unset_jwt_cookies
from common.utils import profile as prof

import sqlite3



user = Blueprint("user", __name__)

@user.route("/register", methods=["POST"])
def register():
    data = request.get_json()
    con = sqlite3.connect("./data.sqlite", check_same_thread=False)
    cur = con.cursor()
    if not data or not data["username"] or not data["password"]:
        return jsonify({"message": "could not register user"}), 400
    hased_pass = generate_password_hash(data["password"], method="sha256")
    try:
        cur.execute(
            "INSERT INTO user(name,username,password)values(?,?,?)",
            (data["name"], data["username"], hased_pass),
        )
        con.commit()
        return jsonify({"message": f"New user, {data['username']} has been created"})
    except:
        print("could not register user in db")
        return jsonify({"message": "could not register user"}), 400
    finally:
        con.close()


@user.route("/login", methods=["POST"])
def login():
    data = request.get_json()
    con = sqlite3.connect("./data.sqlite", check_same_thread=False)
    cur = con.cursor()
    if not data or not data["username"] or not data["password"]:
        return {"message": "Could not verify invalid username/password"}, 400
    try:
        cur.execute("SELECT * FROM user WHERE username = ?", [data["username"]])
        user = cur.fetchone()
        if not user:
            return {"message": "User not Found"}, 404
        if check_password_hash(user[3], data["password"]):
            access_token = create_access_token(
                identity=data["username"],
                additional_claims={"username": data["username"]},
            )
            response = jsonify(
                {"msg": "login successful", "access_token": access_token}
            )
            set_access_cookies(response, access_token)
            return response
    except Exception as e:
        print("could not verify, exception:", e)
        return {"message": "Could not verify"}, 400
    finally:
        con.close()

@user.route("/logout", methods=["POST"])
def logout():
    response = jsonify({"msg": "logout successful"})
    unset_jwt_cookies(response)
    return response

@user.route("/profile", methods=["GET"])
def profile():
    try:
        return prof
    except Exception as e:
        print("exception:", e)
        return {"message": "Could not load profile"}, 400