import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";

import dataReducer from './dataReducer'
import holdingsReducer from "./holdingsReducer";
import ordersReducer from "./ordersReduce";
import profileReducer from "./profile.Reducer"
import authReducer from "./authReducer";
import toastReducer from "./toastReducer";
import { toast } from "react-toastify";

export default combineReducers({
  form: formReducer,
  data: dataReducer,
  holdings: holdingsReducer,
  orders: ordersReducer,
  profile: profileReducer,
  auth: authReducer,
  toast:toastReducer
});
