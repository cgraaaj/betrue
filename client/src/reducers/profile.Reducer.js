import {GET_PROFILE} from "../actions/types"

const INTIAL_STATE = {
    data:{}
}

const profileReducer = (state=INTIAL_STATE,action)=>{
    switch(action.type){
        case GET_PROFILE:
        return{
            ...state,data:action.payload.data
        }
        default:
            return state
    }
}

export default profileReducer