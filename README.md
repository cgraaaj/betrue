# Getting Started with BeTrue Stock Brokerage Client

## About this Project

This project is a Single Page application developed using react, python, and SQLite for #BeTrue clients.

# Available Features - Frontend

The basic functionalities available for the client are as follows
- Register
- Login
- View Dashboard
    - Holdings
    - Chart Analysis (Historic Prices of Nifty 50 and Bank Nifty)
    - Profile info
    - Order

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.


### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!


# Available Features - Backend


The Backend is built using flask python to support the client-side of the application

The endpoints supported by the backend are as follows

- User Authentication
    - POST user/login
    - POST user/logout
    - POST user/register

- Dashboard
    - GET historical-data
    - GET user/profile
    - GET portfolio/holdings
    - POST order/place_order

## ToDo
- websockets

## Available Scripts

### `pip3 install -r requirements.txt`

Installs the required packages

### `python3 app.py`

Runs the backend server on your local. 

# Deployment

## Docker

Docker images have been pushed to the docker repo which is publically accessible

Create a common network in docker, which acts as a bridge to make the client-server and Nginx connected

- Network
    - docker network create betrue

Run the docker images as instructed below on your local 

- Client
    - docker run -d --name client --network=betrue raju6713/betrue_frontend:amd64
- Server
    - docker run -d -p 5000:5000 --name api --network=betrue raju6713/betrue_backend:amd64
- Nginx
    - docker run -d -p 8080:80 --name ngx --network=betrue raju6713/betrue_nginx:amd64 


**_NOTE:_**  Please make sure no other containers running on the name client, api, ngx on your docker network


## K8s
<a href="https://gitlab.com/cgraaaj/betrue-k8s" target="_blank">BeTrue K8s deployment</a>
