import React from "react";
import { connect } from "react-redux";
import _ from "lodash";
import {  } from "../../actions";
import { Form, Field, FormSpy } from "react-final-form";
import { Button, Checkbox } from "semantic-ui-react";
import {register,onClickBack} from "../../actions"

class Register extends React.Component {
  componentDidMount() {}

  onClickRegister = (values) => {
    console.log(values);
    this.props.register(values)
  };

  renderError({ error, touched }) {
    if (touched && error) {
      return (
        <div className="header" style={{ color: "#9f3a38" }}>
          {error}
        </div>
      );
    }
  }

  validate = (formValues) => {
    const errors = {};
    if (!formValues.name) {
      errors.name = "Name required";
    }
    if (!formValues.username) {
      errors.username = "Username required";
    }
    if (!formValues.password) {
      errors.password = "Password required";
    }
    return errors;
  };

  renderInput = ({ input, label, meta }) => {
    const className = `field ${meta.error && meta.touched ? "error" : " "}`;
    return (
      <div className="row">
        <div className="column">
          <div className={`ui right aligned container ${className}`}>
            <label>{label}</label>
          </div>
        </div>
        <div className="column">
          <input
            {...input}
            autoComplete="off"
            type={label === "Password" ? "password" : null}
            id={label}
          />
        {this.renderError(meta)}
        </div>
      </div>
    );
  };

  render() {
    return (
      <div className="ui middle aligned center aligned grid">
        <div className="ui segments">
          <div className="ui segment">
            <div className="ui one column centered grid">
              <div className="row">
                <h4 style={{ margin: "10px" }}>Sign Up</h4>
              </div>
            </div>
          </div>
          <div className="ui segment">
            <div className="ui one column centered grid">
              <div className="column">
                <div className="ui segment">
                  <Form
                    onSubmit={this.onClickRegister}
                    initialValues={this.props.initialValues}
                    validate={this.validate}
                    render={({ handleSubmit, values }) => (
                      <form className="ui form error" onSubmit={handleSubmit}>
                        <div className="ui two column grid container">
                        <Field
                              name="name"
                              component={this.renderInput}
                              label="Name"
                            />
                            <Field
                              name="username"
                              component={this.renderInput}
                              label="Username"
                            />
                            <Field
                              name="password"
                              component={this.renderInput}
                              label="Password"
                            />
                            
                            <div className="row">
                              <div className="column"></div>
                              <div className="column">
                                <Checkbox
                                  label="Show Password"
                                  onClick={(e,data) => {
                                    let passEl =
                                      document.getElementById("Password");
                                    if (data.checked) {
                                      passEl.type = "text";
                                    } else {
                                      passEl.type = "password";
                                    }
                                  }}
                                />
                              </div>
                            </div>
                            <div className="row">
                              <div className="column">
                                <div className="ui right aligned container">
                                  <Button
                                    type="button"
                                    variant="contained"
                                    color="secondary"
                                    onClick={()=>{this.props.onClickBack()}}
                                  >
                                    Back
                                  </Button>
                                </div>
                              </div>
                              <div className="column">
                                <Button type="submit" variant="contained">
                                  Register
                                </Button>
                              </div>
                            </div>
                        </div>
                      </form>
                    )}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    initialValues: {},
  };
};

export default connect(mapStateToProps, {
  register,
  onClickBack
})(Register);
