import React from "react";
import { connect } from "react-redux";
import { Route, Router,Redirect } from "react-router-dom";
import {ToastContainer,toast} from "react-toastify"


import history from "../history";
import Dashboard from "../components/Dashboard";
import Login from "./Login/Login";
import Register from "./Login/Register";



class App extends React.Component {
  componentDidMount(){

  }

  componentDidUpdate(prevProps){
    const tost = this.props.toast
    if(prevProps.toast!==tost)
    {
      console.log(this.props.toast)
      switch(tost.type){
        case 'warn':
          toast.warn(tost.message,tost.options)
        case 'info':
          toast.info(tost.message,tost.options)
        case 'success':
          toast.success(tost.message,tost.options)
        default:
          toast(tost.message,tost.options)
      }
    }
  }

  render() {
    return this.props.isSignedIn ? (
      <div className="ui container" style={{ width: "98%" }}>
        {/* <ToastContainer /> */}
        <Router history={history}>
          <div>
            <Route path={`/login`} component={Login}></Route>
            <Route path={`/register`} component={Register}></Route>
            <Dashboard />
          </div>
        </Router>
      </div>
    ) : (
      <div>
        <Router history={history}>
          <Route path={`/`} exact component={Login}></Route>
          <Route path={`/register`} component={Register}></Route>
          <Redirect path={`/`} exact component={Login}></Redirect>
        </Router>
      </div>
    );
  }
}

const mapStatetoProps = (state) => {
  return { initialValues: {}, isSignedIn: state.auth.isSignedIn,
toast:state.toast
};
};

export default connect(mapStatetoProps)(App);
